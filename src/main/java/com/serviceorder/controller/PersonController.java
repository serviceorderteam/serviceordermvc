package com.serviceorder.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.serviceorder.model.Person;
import com.serviceorder.service.PersonService;

@Controller
@SessionAttributes("Person")
public class PersonController {

	@Autowired
	private PersonService personService;

	// @RequestMapping(value="/addPerson", method = RequestMethod.GET)
	// public String addPerson(@Valid @ModelAttribute ("person") Person person)
	//
	// {
	//
	//
	// System.out.println("Imi� to : " + person.getName() + ", Nazwisko to: " +
	// person.getSurname());
	// personService.save(person);
	// return "addPerson";
	// }

	@RequestMapping(value = "/addPerson", method = RequestMethod.GET)
	public String addPerson(Model model, HttpSession session) {
		Person person = (Person) session.getAttribute("Person");
		

		if (person == null) {
			person = new Person();
			person.setName("Janusz");
			person.setSurname("teszjanusz");
		}

		model.addAttribute("person", person);
		System.out.println("get : " + person.getName());
		return "addPerson";

	}

	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String updatePerson(@Valid @ModelAttribute("person") Person person, BindingResult result)

	{
		if(result.hasErrors())
		{
			return "addPerson";
		}
		else
		{
			personService.save(person);
		}
		
		System.out.println("post : " + person.getName());
		return "redirect:addPerson.html";
	}

	@RequestMapping(value = "/addPersonJson", method = RequestMethod.POST)
	@ResponseBody
	public String addPersonFromJSon(@RequestBody Person person) {
		personService.save(person);
		return "redirect:index.jsp";
	}

	@RequestMapping(value = "/addPersonJson", method = RequestMethod.GET)
	public @ResponseBody Person addPersonJson(@ModelAttribute("person") Person person)

	{

		Person person1 = new Person();

		person1.setName("adam");
		person1.setSurname("dupka");

		return person1;
	}
}
