package com.serviceorder.service;

import com.serviceorder.model.Person;

public interface PersonService {

	Person save(Person person);
	
}
