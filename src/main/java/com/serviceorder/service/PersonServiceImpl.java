package com.serviceorder.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.serviceorder.model.Person;
import com.serviceorder.repository.PersonRepository;

@Service("personService")
public class PersonServiceImpl implements PersonService {

	@Autowired
	@Qualifier("personRepository")
	private PersonRepository personRepository;
	
	
	@Transactional
	public Person save(Person person) {
		return personRepository.save(person);
	}

}
