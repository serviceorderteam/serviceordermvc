package com.serviceorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.serviceorder.model.Person;


@Repository("personRepository")
public interface PersonRepository extends JpaRepository<Person, Long> {

}
